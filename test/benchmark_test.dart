import 'package:test/test.dart';

import 'package:benchmark/benchmark.dart' as benchmark;

class DataSet {
  final _data = <String, int>{};

  void add(String key) {
    var obj = _data[key] ?? 0;
    _data[key] = obj + 1;
  }

  int retrieve(String key) => _data[key] ?? 0;
}

void main() {
  setUp(() => benchmark.reset());

  group('benchmark', () {
    test('The duration should be used when passed', () {
      final watch = Stopwatch();

      benchmark.setUp(() => watch.start());
      benchmark.tearDown(() => watch.stop());

      benchmark.benchmark(
        'benchmark',
        () {},
        duration: Duration(seconds: 3),
      );

      benchmark.measure();

      expect(watch.elapsedMilliseconds, greaterThanOrEqualTo(3000));
    });
  });

  group('setUp methods', () {
    test('The setUpAll should be called in the right order', () {
      final result = DataSet();

      benchmark.setUpAll(() => result.add('firstSetUpAll'));

      benchmark.group('sub group', () {
        benchmark.setUpAll(() => result.add('secondSetUpAll'));

        benchmark.benchmark('benchmark', () {}, iterations: 0);
      });

      benchmark.measure();

      expect(result.retrieve('firstSetUpAll'), equals(1),
          reason: 'The first setUpAll should be called once');
      expect(result.retrieve('secondSetUpAll'), equals(1),
          reason: 'The second setUpAll should be called once');
    });

    test('The setUp should be called before benchmarks, and for each benchmark',
        () {
      final result = DataSet();

      benchmark.setUp(() => result.add('firstSetUp'));

      benchmark.group('sub group', () {
        benchmark.setUp(() => result.add('secondSetUp'));

        benchmark.benchmark('benchmark', () {}, iterations: 0);
      });

      benchmark.measure();

      expect(result.retrieve('firstSetUp'), equals(1),
          reason: 'The first setUp should be called once');
      expect(result.retrieve('secondSetUp'), equals(1),
          reason: 'The second setUp should be called once');
    });

    //   test('The setUpEach should be called before iteration of a benchmark', () {
    //     final result = <String, int>{
    //       'firstSetUpEach': 0,
    //       'secondSetUpEach': 0,
    //     };

    //     benchmark.setUpEach(() => result['firstSetUpEach']++);

    //     benchmark.group('sub group', () {
    //       benchmark.setUpEach(() => result['secondSetUpEach']++);

    //       benchmark.benchmark(
    //         'benchmark',
    //         () {},
    //         iterations: 0,
    //         duration: Duration(microseconds: 0),
    //       );
    //     });

    //     benchmark.measure();

    //     expect(result['firstSetUpEach'], 714811,
    //         reason: 'The first setUpEach should be called 714811 times');
    //     expect(result['secondSetUpEach'], 714811,
    //         reason: 'The second setUpEach should be called 714811 times');
    //   });
  });

  group('tearDown methods', () {
    test('The tearDownAll should be called in the right order', () {
      final result = DataSet();

      benchmark.tearDownAll(() => result.add('firstTearDownAll'));

      benchmark.group('sub group', () {
        benchmark.tearDownAll(() => result.add('secondTearDownAll'));

        benchmark.benchmark('benchmark', () {}, iterations: 0);
      });

      benchmark.measure();

      expect(result.retrieve('firstTearDownAll'), equals(1),
          reason: 'The first tearDownAll should be called once');
      expect(result.retrieve('secondTearDownAll'), equals(1),
          reason: 'The second tearDownAll should be called once');
    });

    test(
        'The tearDown should be called after benchmarks, and for each benchmark',
        () {
      final result = DataSet();

      benchmark.tearDown(() => result.add('firstTearDown'));

      benchmark.group('sub group', () {
        benchmark.tearDown(() => result.add('secondTearDown'));

        benchmark.benchmark('benchmark', () {}, iterations: 0);
      });

      benchmark.measure();

      expect(result.retrieve('firstTearDown'), equals(1),
          reason: 'The first tearDown should be called once');
      expect(result.retrieve('secondTearDown'), equals(1),
          reason: 'The second tearDown should be called once');
    });
  });
}
