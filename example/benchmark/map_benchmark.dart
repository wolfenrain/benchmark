import 'package:benchmark/benchmark.dart';

void main() {
  group('Map', () {
    group('factory', () {
      benchmark('.unmodifiable(other)', () {
        Map.unmodifiable({'value': 1});
      }, iterations: 100);
    });
  });
}
