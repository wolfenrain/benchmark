import 'package:benchmark/benchmark.dart';

/// This is just a example benchmark. Normally you would test
/// the different kind of Lists.
void main() {
  group('List', () {
    group('factory', () {
      List<int>? list;

      setUp(() => list = List.generate(100000, (i) => i));

      tearDown(() => list = null);

      benchmark('.generate(100k, growable: true)', () {
        List.generate(100000, (i) => i, growable: true);
      });

      benchmark('.generate(100k, growable: false)', () {
        List.generate(100000, (i) => i, growable: false);
      });

      benchmark('.from(elements, growable: true)', () {
        List.from(list!, growable: true);
      });

      benchmark('.from(elements, growable: false)', () {
        List.from(list!, growable: false);
      });

      benchmark('.of(elements, growable: true)', () {
        List.of(list!, growable: true);
      });

      benchmark('.of(elements, growable: false)', () {
        List.of(list!, growable: false);
      });

      benchmark('.empty(growable: true)', () {
        List.empty(growable: true);
      });

      benchmark('.empty(growable: false)', () {
        List.empty(growable: false);
      });
    });

    group('instance', () {
      List<int>? list;

      setUp(() => list = List.generate(100000, (i) => i));

      tearDown(() => list = null);

      benchmark('.forEach()', () {
        list?.forEach((i) {});
      });

      benchmark('.toList(growable: true)', () {
        list?.toList(growable: true);
      });

      benchmark('.toList(growable: false)', () {
        list?.toList(growable: false);
      });

      benchmark('operator ...(List)', () {
        [...list!];
      });
    });

    // group('instance', () {
    //   List<int> original = List.generate(100000, (i) => i);
    //   List<int> list;

    //   setUpEach(() => list = List.from(original));

    //   tearDownEach(() => list = null);

    //   benchmark('.map()', () {
    //     list.map((i) => '$i');
    //   }, duration: Duration(milliseconds: 100));
    // });
  });
}
