import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:args/args.dart';

final benchmarks = <Map<String, dynamic>>[];
var isVerbose = false;
final ansiSupport = stdout.supportsAnsiEscapes;

String group(List<List<String>> items) {
  final keyLength =
      items.fold<int>(0, (v, i) => v < i[0].length ? i[0].length : v) + 1;
  return items.map((item) {
    return '${item.first}:'.padRight(keyLength, ' ') + ' ${item.last}';
  }).join('\n');
}

String color(String message, int color, {bool bg = false}) {
  return ansiSupport
      ? '\x1B[${bg ? 48 : 38};5;${color}m$message\x1B[0m'
      : message;
}

String black(String message, {bool bg = false}) => color(message, 0, bg: bg);
String white(String message, {bool bg = false}) => color(message, 15, bg: bg);
String red(String message, {bool bg = false}) => color(message, 1, bg: bg);
String green(String message, {bool bg = false}) => color(message, 2, bg: bg);
String yellow(String message, {bool bg = false}) => color(message, 3, bg: bg);
String grey(String message, {bool bg = false}) => color(message, 8, bg: bg);

void debug(String message) => isVerbose ? print(grey(message)) : null;
void info(String message) => print(message);
void warn(String message) => print(yellow(message));
void error(String message) => print(red(message));

void main(List<String> arguments) async {
  final parser = ArgParser();
  parser.addFlag(
    'help',
    abbr: 'h',
    help: 'Print this usage information.',
    negatable: false,
  );
  parser.addFlag('verbose', help: 'Show verbose logging', negatable: false);

  parser.addOption(
    'dir',
    help: 'The directory in which to look for benchmarks.',
    valueHelp: 'String',
    defaultsTo: './benchmark',
  );

  final result = parser.parse(arguments);
  isVerbose = result['verbose'];

  if (result['help']) {
    info('Run your benchmarks easily.\n');
    info('Usage: benchmark [arguments]\n');
    return print(parser.usage);
  }

  final uri = Uri.parse((result['dir'] ?? './benchmark'));
  final directory = uri.hasAbsolutePath
      ? Directory.fromUri(uri)
      : Directory(Directory.current.path + '/' + uri.path);

  debug(group([
    ['Dart version', Platform.version],
    ['Location', Platform.script.path],
    ['Working directory', Directory.current.path],
    ['Benchmark directory', directory.path],
  ]));
  debug('');

  if (!await directory.exists()) {
    return error('Directory $directory does not exist!');
  }

  final benchmarkSuites = directory.listSync(recursive: true)
    ..retainWhere(
      (element) =>
          element.statSync().type == FileSystemEntityType.file &&
          element.path.endsWith('_benchmark.dart'),
    );

  var passedSuites = 0;
  final watch = Stopwatch()..start();
  for (final benchmarkSuite in benchmarkSuites) {
    final startTime = watch.elapsed;
    final currentPath = benchmarkSuite.path
        .replaceFirst(Directory.current.path, '.')
        .split('/');
    final fileName = currentPath.removeLast();
    final path = '${grey(currentPath.join('/') + '/')}$fileName';
    final len = path.length;

    stdout.write('${yellow(black(' RUNNING '), bg: true)} $path');

    try {
      final result = await runBenchmark(benchmarkSuite.path);
      final endTime = watch.elapsed - startTime;
      if (!checkIfFailed(result)) {
        passedSuites++;
        stdout.write(
            '\x1B[${len + 10}D${green(black(' DONE '), bg: true)} $path ${grey('(${durationToString(endTime)})')}\n');
      } else {
        stdout.write(
            '\x1B[${len + 10}D${red(white(' FAILED '), bg: true)} $path ${grey('(${durationToString(endTime)})')}\n');
      }
      printMap(result, 1);
    } catch (err) {
      stdout.write(
          '[${len + 10}D${red(white(' ERROR '), bg: true)} $path     \n');
      error('$err');
    }
    info('');
  }
  watch.stop();

  info(group([
    [
      'Benchmark suites',
      '${green('$passedSuites passed')}, ${benchmarkSuites.length} total'
    ],
    [
      'Benchmarks',
      '${green('${benchmarks.where((b) => !b['isFailure']).length} passed')}, ${benchmarks.length} total'
    ],
    ['Time', green(durationToString(watch.elapsed))]
  ]));
  info(grey('Ran all benchmark suites.'));
}

bool checkIfFailed(Map<String, dynamic> map) {
  var isFailure = false;
  for (final key in map.keys) {
    if (map[key] is Map) {
      if (map[key].containsKey('isGroup')) {
        isFailure = isFailure || checkIfFailed(map[key]);
      } else {
        isFailure = isFailure || map[key]['isFailure'];
      }
    }
  }
  return isFailure;
}

void printMap(Map<String, dynamic> map, [int indenting = 0]) {
  final padding = ''.padRight(indenting, ' ');
  for (final key in map.keys) {
    if (map[key] is Map) {
      if (map[key].containsKey('isGroup')) {
        info(''.padLeft(indenting, ' ') + key);
        printMap(map[key], indenting + 1);
      } else {
        benchmarks.add(map[key]);
        final measure = Duration(microseconds: map[key]['measure'].toInt());
        final measureStr = durationToString(measure);
        if (map[key]['isFailure']) {
          info('$padding${red('×')} ${grey('$key ($measureStr)')}');
          info('$padding  ${red(map[key]['error'])}');
        } else {
          info('$padding${green('✓')} ${grey('$key ($measureStr)')}');
          debug(group([
            [
              '$padding  Elapsed',
              '${durationToString(Duration(microseconds: map[key]['elapsed']))}'
            ],
            ['$padding  Rounds', '${map[key]['rounds']}'],
            ['$padding  Iterations', '${map[key]['iterations']}'],
          ]));
        }
      }
    }
  }
}

String durationToString(Duration duration) {
  return duration.inSeconds > 0
      ? '${duration.inSeconds} s'
      : duration.inMilliseconds > 0
          ? '${duration.inMilliseconds} ms'
          : '${duration.inMicroseconds} us';
}

Future<Map<String, dynamic>> runBenchmark(String benchmarkFile) async {
  final result = await Process.run('dart', ['-Dbenchmark=true', benchmarkFile]);
  return jsonDecode(result.stdout);
}
