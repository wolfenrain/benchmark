[![plugin version](https://img.shields.io/pub/v/benchmark?label=pub)](https://pub.dev/packages/benchmark)
[![coverage report](https://gitlab.com/wolfenrain/benchmark/badges/master/coverage.svg)](https://gitlab.com/wolfenrain/benchmark/-/commits/master)
[![pipeline status](https://gitlab.com/wolfenrain/benchmark/badges/master/pipeline.svg)](https://gitlab.com/wolfenrain/benchmark/-/commits/master)
[![dependencies](https://img.shields.io/librariesio/release/pub/benchmark?label=dependencies)](https://gitlab.com/wolfenrain/benchmark/-/blob/master/pubspec.yaml)

# benchmark

`benchmark` provides a standard way of writing and running benchmarks in Dart. Inspired by both [benchmark_harness](https://pub.dev/packages/benchmark_harness) and [test](https://pub.dev/packages/test).

## Writing Benchmarks.

Benchmarks are specified using the top-level `benchmark()` function:
```dart
import 'package:benchmark/benchmark.dart';

void main() {
  benchmark('List.generate(100k)', () {
    List.generate(100000, (i) => i);
  });
}
```

Benchmarks can be grouped together using the `group()` function. Each group's description is added to the beginning of its benchmark's descriptions:
```dart
import 'package:benchmark/benchmark.dart';

void main() {
  group('List', () {
    benchmark('.generate(100k)', () {
      List.generate(100000, (i) => i);
    });
  });

  group('Map', () {
    benchmark('.unmodifiable(other)', () {
      Map.unmodifiable({
        'value': 1
      });
    }, iterations: 100);
  });
}
```
Groups at the same level, with the same name, will be merged into one, allowing for different benchmark setups, without losing the overview in the output.

You can use the `setUp()` and `tearDown()` functions to share code between benchmarks. The `setUp()` callback will run before every benchmark in a group, and `tearDown()` will run after:
```dart
import 'package:benchmark/benchmark.dart';

void main() {
  List<int> list;

  setUp(() => list = List.generate(100000, (i) => i));

  tearDown(() => list = null);

  benchmark('List(100k).forEach()', () {
    list.forEach((i) {});
  });
}
```

You can use the `setUpEach()` and `tearDownEach()` functions to share code between iterations of benchmarks. The `setUpEach()` callback will run before every iteration in a benchmark, and `tearDownEach()` will run after:
```dart
import 'package:benchmark/benchmark.dart';

void main() {
  List<int> list;

  setUpEach(() => list = List.generate(100000, (i) => i));

  tearDownEach(() => list = null);

  benchmark('List(100k).map()', () {
    list.map((i) => '$i');
  });
}
```

## Running benchmarks

`benchmark` comes with a simple executable to automatically run all your benchmarks:
```bash
pub run benchmark
```

See the `--help` flag for more information.
