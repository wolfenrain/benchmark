## 0.3.0

- Stable null safety release.

## 0.2.1

- Groups with the same name on the same level will be merged together.
- Benchmark can now be run in environments without a terminal attached, for instance in a CI/CD.

## 0.2.0

- **BREAKING**: Removed the `report()` function in favour of the command line tool.
- **BREAKING**: Removed both `GroupEntry` and `BenchmarkRunner` classes as parts from the library, they are not useful outside of the library.
- Improved the command line tool, beter visual output.

## 0.1.0

- Added `group()` function.
- Added `benchmark()` function.
- Added `setUp()` function.
- Added `setUpEach()` function.
- Added `setUpAll()` function.
- Added `tearDown()` function.
- Added `tearDownEach()` function.
- Added `tearDownAll()` function.
