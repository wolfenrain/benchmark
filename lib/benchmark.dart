/// A full featured library for writing and running Dart benchmarks.
library benchmark;

import 'package:meta/meta.dart';
import './src/group_entry.dart';

GroupEntry _g = GroupEntry('', null);

/// Creates a new benchmark with the given [name] and [callback].
///
/// A benchmark will run for a certain [duration], it will execute the [callback]
/// times the amount of [iterations]. That is one execution call, and the time it takes to execute
/// will then be added to the elapsed time.
///
/// When the elapsed time is equal or greater than the [duration] it will stop and calculate the benchmark time:
/// `performanceTime = elapsedTime / whileIterationsDone`.
///
/// The [name] will be added to the names of any surrounding
/// [group]s.
///
/// If [iterations] is passed, it will be used as the amount of iterations this
/// benchmark will use for execution, by default it will be 10 iterations.
///
/// If [duration] is passed, it will be used as the time that the benchmark has to run,
/// it will execute the benchmark as long as the time hasn't passed, by default it will be 2 seconds.
void benchmark(
  String name,
  void Function() callback, {
  int iterations = 10,
  Duration duration = const Duration(seconds: 2),
}) {
  _g.benchmark(name, callback, iterations: iterations, duration: duration);
}

/// Creates a group of benchmarks.
///
/// A group's name is included in the names of any benchmarks
/// or sub-groups it contains. [setUp]/[setUpEach] and [tearDown]/[tearDownEach] are also scoped
/// to the containing group.
void group(String name, void Function() callback) => _g.group(name, callback);

/// Registers a function to be run before the benchmarks.
///
/// This function will be called before each benchmark is run. If you want to run before
/// each iteration of a benchmark see [setUpEach].
///
/// If this is called within a group, it applies only to benchmarks in that
/// group. The [callback] will be run after any set-up callbacks in parent groups or
/// at the top level.
///
/// Each callback at the top level or in a given group will be run in the order
/// they were declared.
void setUp(void Function() callback) => _g.setUp(callback);

/// Register a function to be run before each iteration of a benchmark.
///
/// This function will be called before each iteration of a benchmark is run.
///
/// If this is called within a group, it applies only to benchmarks in that
/// group. The [callback] will be run after any set-up callbacks in parent groups or
/// at the top level.
///
/// Each callback at the top level or in a given group will be run in the order
/// they were declared.
void setUpEach(void Function() callback) => _g.setUpEach(callback);

/// Registers a function to be run once before all benchmarks.
///
/// If this is called within a group, The [callback] will run before all benchmarks
/// in that group. It will be run after any [setUpAll] callbacks in parent
/// groups or at the top level.
///
/// **Note**: This function makes it very easy to accidentally introduce hidden
/// dependencies between benchmarks that should be isolated. In general, you should
/// prefer [setUp], and only use [setUpAll] if the callback is prohibitively
/// slow.
void setUpAll(void Function() callback) => _g.setUpAll(callback);

/// Register a function to be run after each iteration of a benchmark.
///
/// This function will be called after each iteration of a benchmark is run.
///
/// If this is called within a group, it applies only to tests in that
/// group. The [callback] will be run before any tear-down callbacks in parent
/// groups or at the top level.
///
/// Each callback at the top level or in a given group will be run in the
/// reverse of the order they were declared.
void tearDown(void Function() callback) => _g.tearDown(callback);

/// Registers a function to be run after benchmarks.
///
/// This function will be called after each benchmark is run. If you want to run after
/// each iteration of a benchmark see [tearDownEach].
///
/// If this is called within a group, it applies only to tests in that
/// group. The [callback] will be run before any tear-down callbacks in parent
/// groups or at the top level.
///
/// Each callback at the top level or in a given group will be run in the
/// reverse of the order they were declared.
void tearDownEach(void Function() callback) => _g.tearDownEach(callback);

/// Registers a function to be run once after all benchmarks.
///
/// If this is called within a test group, [callback] will run after all benchmarks
/// in that group. It will be run before any [tearDownAll] callbacks in parent
/// groups or at the top level.
///
/// **Note**: This function makes it very easy to accidentally introduce hidden
/// dependencies between benchmarks that should be isolated. In general, you should
/// prefer [tearDown], and only use [tearDownAll] if the callback is
/// prohibitively slow.
void tearDownAll(void Function() callback) => _g.tearDownAll(callback);

@visibleForTesting
void measure() => _g.measure();

@visibleForTesting
void reset() => _g = GroupEntry('', null);
