import 'group_entry.dart';

class BenchmarkRunner {
  final String name;

  final GroupEntry _group;

  final void Function() benchmark;

  final int iterations;

  final Duration duration;

  BenchmarkRunner(
    this.name,
    this._group,
    this.benchmark, {
    this.iterations = 10,
    this.duration = const Duration(seconds: 2),
  })  : assert(iterations != null),
        assert(iterations >= 0),
        assert(duration != null);

  void warmup() => run();

  void exercise() {
    for (var i = 0; i < iterations; i++) {
      run();
    }
  }

  void setUp(GroupEntry forGroup) {
    if (forGroup.parent != null) {
      setUp(forGroup.parent!);
    }
    for (final setUp in forGroup.setUps) {
      setUp();
    }
  }

  void setUpEach(GroupEntry forGroup) {
    if (forGroup.parent != null) {
      setUpEach(forGroup.parent!);
    }
    for (final setUpEach in forGroup.setUpEachs) {
      setUpEach();
    }
  }

  void tearDown(GroupEntry forGroup) {
    for (final tearDown in forGroup.tearDowns) {
      tearDown();
    }
    if (forGroup.parent != null) {
      tearDown(forGroup.parent!);
    }
  }

  void tearDownEach(GroupEntry forGroup) {
    for (final tearDownEach in forGroup.tearDownEachs) {
      tearDownEach();
    }
    if (forGroup.parent != null) {
      tearDownEach(forGroup.parent!);
    }
  }

  Map<String, dynamic> measureFor(Function f, int minimumMillis) {
    var minimumMicros = minimumMillis * 1000,
        rounds = 0,
        watch = Stopwatch(),
        elapsed = 0,
        error;

    try {
      while (elapsed < minimumMicros) {
        setUpEach(_group);

        watch.start();
        f();
        watch.stop();

        tearDownEach(_group);

        elapsed = watch.elapsedMicroseconds;
        rounds++;
      }
    } catch (err) {
      error = err;
      watch.stop();
    }

    return {
      'rounds': rounds,
      'elapsed': elapsed,
      'measure': rounds == 0 ? 0 : elapsed / rounds,
      'iterations': iterations,
      'isFailure': error != null,
      'error': error.toString(),
    };
  }

  Map<String, dynamic> measure() {
    setUp(_group);

    // Warmup for at least 100ms. Discard result.
    measureFor(warmup, 100);

    // Run the benchmark for at least 2000ms.
    var result = measureFor(exercise, duration.inMilliseconds);

    tearDown(_group);

    return result;
  }

  void run() => benchmark();
}
