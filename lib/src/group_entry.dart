import 'dart:async';
import 'dart:convert';

import './benchmark_runner.dart';

var _hasNoCallback = true;

class GroupEntry {
  final String name;

  final GroupEntry? parent;

  final List<GroupEntry> groups = [];

  final List<BenchmarkRunner> benchmarks = [];

  final List<void Function()> setUps = [];
  final List<void Function()> setUpEachs = [];
  final List<void Function()> setUpAlls = [];

  final List<void Function()> tearDowns = [];
  final List<void Function()> tearDownEachs = [];
  final List<void Function()> tearDownAlls = [];

  GroupEntry(this.name, this.parent);

  void benchmark(
    String name,
    void Function() callback, {
    int iterations = 10,
    Duration duration = const Duration(seconds: 2),
  }) {
    final GroupEntry zonedGroup = Zone.current[#test.group] ?? this;
    zonedGroup.benchmarks.add(BenchmarkRunner(
      name,
      zonedGroup,
      callback,
      iterations: iterations,
      duration: duration,
    ));

    if (Zone.current[#test.group] == null && _hasNoCallback) {
      _hasNoCallback = false;
      if (String.fromEnvironment('benchmark') == 'true') {
        Future.delayed(Duration(milliseconds: 100)).then((_) {
          print(jsonEncode(measure()));
        });
      }
    }
  }

  void group(String name, void Function() callback) {
    final GroupEntry zonedGroup = Zone.current[#test.group] ?? this;
    final group = GroupEntry(name, zonedGroup);
    runZoned(callback, zoneValues: {#test.group: group});
    zonedGroup.groups.add(group);

    if (Zone.current[#test.group] == null && _hasNoCallback) {
      _hasNoCallback = false;
      if (String.fromEnvironment('benchmark') == 'true') {
        Future.delayed(Duration(milliseconds: 100)).then((_) {
          print(jsonEncode(measure()));
        });
      }
    }
  }

  void setUp(void Function() callback) {
    final GroupEntry zonedGroup = Zone.current[#test.group] ?? this;
    zonedGroup.setUps.add(callback);
  }

  void setUpEach(void Function() callback) {
    final GroupEntry zonedGroup = Zone.current[#test.group] ?? this;
    zonedGroup.setUpEachs.add(callback);
  }

  void setUpAll(void Function() callback) {
    final GroupEntry zonedGroup = Zone.current[#test.group] ?? this;
    zonedGroup.setUpAlls.add(callback);
  }

  void tearDown(void Function() callback) {
    final GroupEntry zonedGroup = Zone.current[#test.group] ?? this;
    zonedGroup.tearDowns.add(callback);
  }

  void tearDownEach(void Function() callback) {
    final GroupEntry zonedGroup = Zone.current[#test.group] ?? this;
    zonedGroup.tearDownEachs.add(callback);
  }

  void tearDownAll(void Function() callback) {
    final GroupEntry zonedGroup = Zone.current[#test.group] ?? this;
    zonedGroup.tearDownAlls.add(callback);
  }

  void _setUpAll() {
    for (final setUpAll in setUpAlls) {
      setUpAll();
    }

    for (final group in groups) {
      group._setUpAll();
    }
  }

  void _tearDownAll() {
    for (final tearDownAll in tearDownAlls) {
      tearDownAll();
    }

    for (final group in groups) {
      group._tearDownAll();
    }
  }

  Map<String, dynamic> _benchmark() {
    final report = <String, dynamic>{};
    for (final benchmark in benchmarks) {
      report[benchmark.name] = benchmark.measure();
    }
    for (final group in groups) {
      report[group.name] = {
        'isGroup': true,
        ...?report[group.name],
        ...group._benchmark(),
      };
    }
    return report;
  }

  Map<String, dynamic> measure() {
    _setUpAll();
    final report = _benchmark();
    _tearDownAll();
    return report;
  }
}
